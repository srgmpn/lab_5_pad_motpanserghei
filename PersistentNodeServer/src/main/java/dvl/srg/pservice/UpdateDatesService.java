package dvl.srg.pservice;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dvl.srg.helpers.TimerHelper;
import dvl.srg.helpers.UpdateEmployee;
import dvl.srg.infonode.nodeinfoservice.model.Employee;
import dvl.srg.infonode.request.HttpGetRequest;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by administrator on 12/23/15.
 */
public class UpdateDatesService extends Thread {

    private Employee employee;

    public UpdateDatesService(Employee employee) {
        this.employee = employee;
    }

    @Override
    public void run() {
        System.out.println("[INFO] -------------------------------");
        System.out.println("[INFO] START Update INFO NODE Service ... ");
        while (true) {
            TimerHelper.sleep(3000);

            try {
                String empl = new HttpGetRequest("localhost", 8080).sendGetRequest("/update/employees/");
                //System.out.println(empl);

                Type listType = new TypeToken<List<Employee>>() {}.getType();
                List<Employee> employees = new Gson().fromJson(empl, listType);

                employees.stream().forEach((e) -> {
                    if (e.getServerPort() == employee.getServerPort() && !e.equals(employee)){
                        System.out.println("[INFO] --------------------------------");
                        System.out.println("[UPDATE] Server :  localhost:" + e.getServerPort());
                        System.out.println("[INFO] New value : " + e);
                        UpdateEmployee.updateEmployee(e);
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
