package dvl.srg.helpers;

import dvl.srg.infonode.nodeinfoservice.model.Employee;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;


/**
 * Created by administrator on 12/23/15.
 */
public class UpdateEmployee {

    public static void updateEmployee(Employee employee) {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        Document doc = null;

        try {
            docBuilder = docFactory.newDocumentBuilder();

            doc = docBuilder.parse(UpdateEmployee.class.getResource("/employee.xml").getFile());
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < doc.getElementsByTagName("employee").getLength(); ++i) {

            Node empl = doc.getElementsByTagName("employee").item(i);
            NamedNodeMap attribute = empl.getAttributes();
            Node nodeAttr = attribute.getNamedItem("id");

            if (Integer.parseInt(nodeAttr.getTextContent()) == employee.getServerPort()) {
                NodeList nodeList = empl.getChildNodes();

                for (int j = 0; j < nodeList.getLength(); ++j) {
                    Node element = nodeList.item(j);
                    if ("firstName".equals(element.getNodeName())){
                        element.setTextContent(employee.getFirstName());
                    }

                    if ("lastName".equals(element.getNodeName())){
                        element.setTextContent(employee.getLastName());
                    }

                    if ("department".equals(element.getNodeName())){
                        element.setTextContent(employee.getDepartment());
                    }

                    if ("salary".equals(element.getNodeName())){
                        element.setTextContent(String.valueOf(employee.getSalary()));
                    }
                }
               // break;
            }
        }

        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();

            Transformer transformer = transformerFactory.newTransformer();

            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(new File(UpdateEmployee.class.getResource("/employee.xml").getFile()));
            transformer.transform(domSource, streamResult);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
