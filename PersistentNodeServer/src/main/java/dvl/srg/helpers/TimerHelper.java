package dvl.srg.helpers;

/**
 * Created by administrator on 12/23/15.
 */
public class TimerHelper {

    public static void sleep(long delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
