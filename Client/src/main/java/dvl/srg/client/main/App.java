package dvl.srg.client.main;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dvl.srg.infonode.nodeinfoservice.model.Employee;
import dvl.srg.infonode.request.HttpGetRequest;
import dvl.srg.infonode.request.HttpPostRequest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class App
{

    public static void main(String[] args) throws Exception {

        System.out.println("[INFO] -------------------------------");
        System.out.println("[INFO] -         START Client        -");
        System.out.println("[INFO] Send GET Request ... ");

        Thread.sleep(1000);

        String jsonList = new HttpGetRequest("localhost", 8080).sendGetRequest("/employees/");

        Type listType = new TypeToken<List<Employee>>() {}.getType();
        List<Employee> employees = new Gson().fromJson(jsonList, listType);
        System.out.println("[INFO] Response data: ");
        System.out.println("[INFO] " + employees);

        System.out.println("[INFO] -------------------------------");
        System.out.println("[INFO] Send POST Request ...");

        Thread.sleep(5000);

        List<Employee> updateList = new ArrayList<>();

        employees.stream().forEach((e) -> {
            if (e.getDepartment().equals("Financiar")) {
                e.setDepartment("Aviatic");
                e.setSalary(10000.0);
                updateList.add(e);
            }
        });

        new HttpPostRequest("localhost", 8080).sendPostRequest("/update/employees/", updateList);
        System.out.println("[INFO] Updated data: " + updateList);
        System.out.println("[INFO] -------------------------------");
    }
}
