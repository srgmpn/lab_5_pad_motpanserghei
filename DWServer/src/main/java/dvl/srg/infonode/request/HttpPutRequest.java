package dvl.srg.infonode.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import dvl.srg.infonode.nodeinfoservice.model.Employee;
import org.apache.http.Consts;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by administrator on 12/22/15.
 */
public class HttpPutRequest {

    public void sendPUTRequest(final String url, Employee employee) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectMapper mapper = new ObjectMapper();

        mapper.writeValue(out, employee);

        final byte[] data = out.toByteArray();
        String e = new String(data);
        StringEntity body = new StringEntity(e, ContentType.create("application/json", Consts.UTF_8));

        HttpClient client = HttpClientBuilder.create().build();

        HttpPut put = new HttpPut(url);
        put.setEntity(body);
        client.execute(put);
    }
}
