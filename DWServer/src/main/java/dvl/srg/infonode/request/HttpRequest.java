package dvl.srg.infonode.request;

import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.HttpHost;
import org.apache.http.impl.DefaultBHttpClientConnection;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.protocol.*;

/**
 * Created by administrator on 12/23/15.
 */
public abstract class HttpRequest {

    protected String sHost;
    protected int port;

    protected HttpProcessor httpproc;
    protected HttpRequestExecutor httpexecutor;
    protected HttpCoreContext coreContext;
    protected DefaultBHttpClientConnection conn;
    protected ConnectionReuseStrategy connStrategy;
    protected HttpHost host;

    public HttpRequest(String host, int port) {
        this.sHost = host;
        this.port = port;
    }

    protected void init () {
         httpproc = HttpProcessorBuilder.create()
                .add(new RequestContent())
                .add(new RequestTargetHost())
                .add(new RequestConnControl())
                .add(new RequestUserAgent("Client/1.1"))
                .add(new RequestExpectContinue(true)).build();

        httpexecutor = new HttpRequestExecutor();

        coreContext = HttpCoreContext.create();
        host = new HttpHost(sHost, port);
        coreContext.setTargetHost(host);

        conn = new DefaultBHttpClientConnection(8 * 1024);
        connStrategy = DefaultConnectionReuseStrategy.INSTANCE;
    }
}
