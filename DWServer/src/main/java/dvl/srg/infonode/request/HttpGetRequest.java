package dvl.srg.infonode.request;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by administrator on 12/20/15.
 */
public class HttpGetRequest extends HttpRequest{

    public HttpGetRequest(String host, int port) {
        super(host, port);
        init();
    }

    public String sendGetRequest (String ...targets) throws IOException {

        String result = null;
        try {
            for (String target: targets) {
                if (!conn.isOpen()) {
                    Socket socket = new Socket(host.getHostName(), host.getPort());
                    conn.bind(socket);
                }
                BasicHttpRequest request = new BasicHttpRequest("GET", target);
                System.out.println("[INFO] -------------------------------");
                System.out.println("[INFO] GET Request URI: " + request.getRequestLine().getUri());

                httpexecutor.preProcess(request, httpproc, coreContext);
                HttpResponse response = httpexecutor.execute(request, conn, coreContext);
                httpexecutor.postProcess(response, httpproc, coreContext);

                System.out.println("[INFO] Response: " + response.getStatusLine());
                result = EntityUtils.toString(response.getEntity());
                System.out.println("[INFO] -------------------------------");
                if (!connStrategy.keepAlive(response, coreContext)) {
                    conn.close();
                } else {
                    System.out.println("Connection kept alive...");
                }
            }
        } catch (HttpException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        return result;
    }

    public String sendGETRequest(final String url) throws IOException {

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);

        HttpResponse response = client.execute(request);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }
}
