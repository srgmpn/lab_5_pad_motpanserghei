package dvl.srg.infonode.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import dvl.srg.infonode.nodeinfoservice.model.Employee;
import org.apache.http.Consts;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

/**
 * Created by administrator on 12/20/15.
 */
public class HttpPostRequest extends HttpRequest {

    public HttpPostRequest(String host, int port) {
        super(host, port);
        init();
    }

    public void sendPostRequest (String target, List<Employee> employees) throws IOException, HttpException {
        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectMapper mapper = new ObjectMapper();

            mapper.writeValue(out, employees);

            final byte[] data = out.toByteArray();
            String e = new String(data);
            StringEntity body = new StringEntity(e, ContentType.create("application/json", Consts.UTF_8));

            if (!conn.isOpen()) {
                Socket socket = new Socket(host.getHostName(), host.getPort());
                conn.bind(socket);
            }
            BasicHttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest("POST", target);
            request.setEntity(body);
            System.out.println("[INFO] -------------------------------");
            System.out.println("[INFO] POST Request URI: " + request.getRequestLine().getUri());

            httpexecutor.preProcess(request, httpproc, coreContext);
            HttpResponse response = httpexecutor.execute(request, conn, coreContext);
            httpexecutor.postProcess(response, httpproc, coreContext);

            System.out.println("[INFO] Response: " + response.getStatusLine());
            System.out.println(EntityUtils.toString(response.getEntity()));
            System.out.println("[INFO] -------------------------------");
            if (!connStrategy.keepAlive(response, coreContext)) {
                conn.close();
            } else {
                System.out.println("Connection kept alive...");
            }

        } finally {
            conn.close();
        }
    }
}
