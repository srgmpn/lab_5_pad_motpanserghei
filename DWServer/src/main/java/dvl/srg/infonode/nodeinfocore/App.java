package dvl.srg.infonode.nodeinfocore;

import dvl.srg.infonode.nodehttpservice.NodeController;

import java.io.IOException;

/**
 * Created by administrator on 12/20/15.
 */
public class App {
    public static void main(String[] args) throws IOException {

        System.out.println("[INFO] -------------------------------");
        System.out.println("[INFO] Start Data Warehouse server ... ");

        new NodeController(8080).start();
    }
}
