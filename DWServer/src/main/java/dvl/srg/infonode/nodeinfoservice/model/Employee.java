package dvl.srg.infonode.nodeinfoservice.model;

import java.io.Serializable;

/**
 * Created by administrator on 12/20/15.
 */
public class Employee implements Serializable {

    private static final long serialVersionUID = -8619384790961818604L;

    private String firstName;
    private String lastName;
    private String department;
    private Double salary;
    private int serverPort;

    public Employee(String firstName, String lastName, String departament, Double salary, int serverPort) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = departament;
        this.salary = salary;
        this.serverPort = serverPort;
    }

    public Employee() {
    }

    public Employee(int serverPort) {
        this.serverPort = serverPort;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public int getServerPort() {
        return serverPort;
    }

    @Override
    public boolean equals(Object o) {

        if (o == null || !(o instanceof Employee))
            return false;

        if (o == this)
            return true;

        if (((Employee)o).getServerPort() != serverPort )
            return false;

        if  (!((Employee)o).getFirstName().equals(firstName))
            return false;

        if  (!((Employee)o).getLastName().equals(lastName))
            return false;

        if  (!((Employee)o).getDepartment().equals(department))
            return false;

        if (((Employee)o).getSalary() != salary )
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", department='" + department + '\'' +
                ", salary=" + salary +
                '}';
    }
}
