package dvl.srg.infonode.nodehttpservice;

import dvl.srg.infonode.nodehttpservice.handler.HttpEmployeeHandler;
import dvl.srg.infonode.nodehttpservice.handler.HttpEmployeesHandler;
import dvl.srg.infonode.nodehttpservice.handler.HttpNodesLocationHandler;
import dvl.srg.infonode.nodehttpservice.handler.HttpUpdateEmployeesHandler;
import org.apache.http.protocol.*;

import java.io.IOException;

/**
 * Created by administrator on 12/20/15.
 */
public class NodeController {

    protected int workingPort;

    public NodeController(int workingPort) {
        this.workingPort = workingPort;
    }

    public void start() throws IOException {

        // Set up the HTTP protocol processor
        HttpProcessor httpProcessor = HttpProcessorBuilder.create()
                .add(new ResponseDate())
                .add(new ResponseServer("DIS - InfoNode/1.1"))
                .add(new ResponseContent())
                .add(new ResponseConnControl())
                .build();

        // Set up request handlers
        UriHttpRequestHandlerMapper reqistry = new UriHttpRequestHandlerMapper();
        setRegistry(reqistry, httpProcessor);

        // Set up the HTTP service
        HttpService httpService = new HttpService(httpProcessor, reqistry);

        // Start HTTP listener
        RequestListener.getInstance(workingPort, httpService).start();

    }

    private void setRegistry(UriHttpRequestHandlerMapper reqistry, HttpProcessor httpProcessor) throws IOException {

        reqistry.register("/employees/", new HttpEmployeesHandler());
        reqistry.register("/employee/*", new HttpEmployeeHandler());
        reqistry.register("/locations/", new HttpNodesLocationHandler());
        reqistry.register("/update/employees/", new HttpUpdateEmployeesHandler());

    }
}
