package dvl.srg.infonode.nodehttpservice;

import dvl.srg.infonode.nodeinfoservice.LocationService;
import dvl.srg.infonode.nodeinfoservice.InfoService;
import org.apache.http.HttpConnectionFactory;
import org.apache.http.HttpServerConnection;
import org.apache.http.impl.DefaultBHttpServerConnection;
import org.apache.http.impl.DefaultBHttpServerConnectionFactory;
import org.apache.http.protocol.HttpService;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by administrator on 12/20/15.
 */
public class RequestListener extends Thread {

    private static RequestListener INSTANCE;

    private final HttpConnectionFactory<DefaultBHttpServerConnection> connFactory;

    private final ServerSocket serversocket;
    private final HttpService httpService;
    private final InfoService infoService;
    private final LocationService locationService;
    private final InfoService updateInfoService;

    private RequestListener(
            final int port,
            final HttpService httpService) throws IOException {

        this.connFactory = DefaultBHttpServerConnectionFactory.INSTANCE;
        this.serversocket = new ServerSocket(port);
        this.httpService = httpService;
        infoService = new InfoService();
        locationService = new LocationService();
        updateInfoService = new InfoService();
        this.setDaemon(false);
    }

    public static RequestListener getInstance(final int port, final HttpService httpService) throws IOException {

        if (INSTANCE == null) {
            synchronized (RequestListener.class) {
                if (INSTANCE == null) {
                    INSTANCE = new RequestListener(port, httpService);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void run() {
        System.out.println("[INFO] Listening on port " + this.serversocket.getLocalPort());
        while (!Thread.interrupted()) {
            try {
                // Set up HTTP connection
                Socket socket = this.serversocket.accept();
                System.out.println("[INFO] ----------------------------------------------- ");
                System.out.println("[INFO] Incoming connection from " + socket.getInetAddress());
                System.out.println("[INFO] ----------------------------------------------- ");
                HttpServerConnection conn = this.connFactory.createConnection(socket);

                // Start worker thread
                new Worker(this.httpService, conn, infoService, locationService, updateInfoService).start();

            } catch (InterruptedIOException ex) {
                break;
            } catch (IOException e) {
                System.out.println("[ERROR] I/O error initialising connection thread: "
                        + e.getMessage());
                System.out.println("[INFO] ----------------------------------------------- ");
                break;
            }
        }
    }
}
