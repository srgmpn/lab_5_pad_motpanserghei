package dvl.srg.infonode.nodehttpservice.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import dvl.srg.infonode.nodeinfoservice.LocationService;
import org.apache.http.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by administrator on 12/20/15.
 */
public class HttpNodesLocationHandler implements HttpRequestHandler {
    @Override
    public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException {
        String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
        LocationService locationService = (LocationService) context.getAttribute("locationService");
/*
        System.out.println("[INFO] ------------------------------------ ");
        System.out.println("[INFO] INTRAT HttpNodesLocationHandler ");
        System.out.println("[INFO] ------------------------------------ ");*/

        if (!method.equals("GET")) {
            throw new MethodNotSupportedException(method + " method not supported");
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectMapper mapper = new ObjectMapper();

        mapper.writeValue(out, locationService.getLocations());

        final byte[] data = out.toByteArray();
        String e = new String(data);

        response.setStatusCode(HttpStatus.SC_OK);
        StringEntity body = new StringEntity(e, ContentType.create("application/json", Consts.UTF_8));
        response.setEntity(body);
        System.out.println("[INFO] Sending bytes length is " + body.getContentLength());
        System.out.println("[INFO] ----------------------------------------------- ");
    }
}
