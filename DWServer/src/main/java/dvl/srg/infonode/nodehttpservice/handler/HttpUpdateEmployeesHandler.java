package dvl.srg.infonode.nodehttpservice.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dvl.srg.infonode.nodeinfoservice.InfoService;
import dvl.srg.infonode.nodeinfoservice.model.Employee;
import org.apache.http.*;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Locale;

/**
 * Created by administrator on 12/22/15.
 */
public class HttpUpdateEmployeesHandler implements HttpRequestHandler {
    @Override
    public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException {

        String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
        InfoService updateInfoService = (InfoService) context.getAttribute("updateInfoService");

       /* System.out.println("[INFO] ----------------------------------------------- ");
        System.out.println("[INFO] INTRAT HttpEmployeesHandler ");
        System.out.println("[INFO] ----------------------------------------------- ");*/


        if (method.equals("POST")) {

            HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();

            if (entity != null) {
                entity = new BufferedHttpEntity(entity);
            }

            byte[] entityContent = EntityUtils.toByteArray(entity);
            System.out.println("[INFO] ----------------------------------------------- ");
            System.out.println("[INFO] Incoming to update entities content (bytes): " + entityContent.length);

            Type listType = new TypeToken<List<Employee>>() {}.getType();
            List<Employee> employees = new Gson().fromJson(EntityUtils.toString(entity), listType);
            employees.stream().forEach((empl) -> updateInfoService.add(empl));

            System.out.println("[INFO] ----------------------------------------------- ");
            response.setStatusCode(HttpStatus.SC_CREATED);
        }

        if (method.equals("GET")) {
            //System.out.println("******[GET] -> updateInfoService: " + updateInfoService.getEmployees());
            if (updateInfoService != null) {

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectMapper mapper = new ObjectMapper();

            mapper.writeValue(out, updateInfoService.getEmployees());

            final byte[] data = out.toByteArray();
            String e = new String(data);

            response.setStatusCode(HttpStatus.SC_OK);
            StringEntity body = new StringEntity(e, ContentType.create("application/json", Consts.UTF_8));
            response.setEntity(body);
            System.out.println("[INFO] Sending bytes length is " + body.getContentLength());
            System.out.println("[INFO] ----------------------------------------------- ");
            } else {
                System.out.println("**********************************");
                response.setStatusCode(HttpStatus.SC_OK);
            }
        }

    }
}
